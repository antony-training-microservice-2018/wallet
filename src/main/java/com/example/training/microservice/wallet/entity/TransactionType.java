package com.artivisi.training.microservice.wallet.entity;

public enum TransactionType {
    TOPUP,PAYMENT,PURCHASE
}
