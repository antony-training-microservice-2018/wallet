package com.artivisi.training.microservice.wallet.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity @Data
public class WalletTransaction {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_wallet")
    private Wallet wallet;

    private LocalDateTime transactionTime = LocalDateTime.now();

    @NotEmpty
    private String description;

    @NotNull @Enumerated(EnumType.STRING)
    private TransactionType transactionType;
}
