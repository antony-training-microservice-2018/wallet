create table wallet_owner (
    id varchar(36),
    name varchar(255) not null,
    email varchar(255) not null,
    mobile_phone varchar(50),
    primary key (id),
    unique(mobile_phone),
    unique(email)
);

create table wallet (
    id varchar(36),
    id_owner varchar (36) not null,
    code varchar(100) not null,
    balance decimal(19,2) not null,
    primary key (id),
    unique(code),
    foreign key (id_owner) references wallet_owner(id)
);

create table wallet_transaction (
    id varchar(36),
    id_wallet varchar(36) not null,
    amount decimal(19,2) not null,
    transaction_time timestamp not null,
    description varchar(255),
    primary key (id),
    foreign key (id_wallet) references wallet(id)
);
